const db = require('../db')


const getAllUser = async(req,res,next) => {
    
    try {
        const promisePool = db.pool.promise();
    
        const [rows,fields] = await promisePool.query("SELECT * from users");
        if(rows.length < 1) {
            return res.json({
                code: 2,
                data: []
            })
        }
        return res.json({
            code: 0,
            data: rows
        })
    } catch (error) {
        return res.json({
            code: 1,
            data: []
        })
    }
}

const getFindByUser = async(req, res, next) => {
    const {id} = req.params

    try {
        const promisePool = db.pool.promise();
    
        const [rows,fields] = await promisePool.query("SELECT * from users where iduser = ?",id);
        if(rows.length < 1) {
            return res.json({
                code: 2,
                data: []
            })
        }
        return res.json({
            code: 0,
            data: rows
        })
    } catch (error) {
        return res.json({
            code: 1,
            data: []
        })
    }
}
const createdUser = async(req, res, next) => {
    const {name,iduser,password} = req.body
    try {
        
        const promisePool = db.pool.promise();
        const querySQL = `INSERT INTO users(name, iduser, password) VALUES (?,?,?)`
        const rs = await promisePool.query(querySQL,[name,iduser,password]);
        return res.json({
            code: 0,
            message: "Success"
        })
        
    } catch (error) {
        return res.json({
            code: 1,
            message: "Error"
        })
    }
}
const getAllRSS = async(req,res ,next) => {
    try {
        
        const promisePool = db.pool.promise();
        const querySQL = `SELECT A.*, B.name FROM news_link as A, local as B where A.id = B.id`
        const [rows] = await promisePool.query(querySQL);
        if(rows.length < 1 ) {
            return res.json({
                code: 2,
                message: "Not found"
            })
        }
        return res.json({
            code: 0,
            data: rows,
            message: "Success"
        })
        
    } catch (error) {
        return res.json({
            code: 1,
            message: "Error"
        })
    }
}

const getTopRSS = async(req,res ,next) => {
    try {
        
        const promisePool = db.pool.promise();
        const querySQL = `SELECT A.*, B.name FROM news_link as A, local as B where A.id = B.id and A.isCheck = 0`
        const [rows] = await promisePool.query(querySQL);
        if(rows.length < 1 ) {
            return res.json({
                code: 2,
                message: "Not found"
            })
        }
        return res.json({
            code: 0,
            data: rows,
            message: "Success"
        })
        
    } catch (error) {
        return res.json({
            code: 1,
            message: "Error"
        })
    }
}

const updateRssUser = async(req,res ,next) => {
    const {id} = req.body
    try {
        
        const promisePool = db.pool.promise();
        const querySQL = `UPDATE news_link SET status = 0 where id= ?`
        await promisePool.query(querySQL,[id]);
        
        return res.json({
            code: 0,
            message: "Success"
        })
        
    } catch (error) {
        return res.json({
            code: 1,
            message: "Error"
        })
    }
}
module.exports = {
    getAllUser,
    getFindByUser,
    createdUser,
    getAllRSS,
    getTopRSS,
    updateRssUser
}