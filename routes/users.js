const db = require('../db')
const weatherController = require('../controllers/weather')
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', weatherController.getAllUser );
router.get('/:id', weatherController.getFindByUser );
router.post('/create', weatherController.createdUser );
module.exports = router;