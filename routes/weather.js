const db = require('../db')
var express = require('express');
var router = express.Router();
const weatherController = require('../controllers/weather')
/* GET users listing. */
router.get('/all-rss',  weatherController.getAllRSS);
router.get('/all-top',  weatherController.getTopRSS);
router.post('/update',  weatherController.updateRssUser);
module.exports = router;